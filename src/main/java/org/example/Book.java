package org.example;

import java.math.BigDecimal;
import java.util.Arrays;

public class Book {

    public int id;
    private static int count = 1;
    public String name;
    public Auteur[] auteurs;
    public Editeur editeur;
    public int publishingYear;
    public int numberPages;
    public BigDecimal prix;
    public CoverType coverType;

    public Book(String name, Auteur[] auteurs, Editeur editeur, int publishingYear, int numberPages, BigDecimal prix, CoverType coverType) {
        this.id = count++;
        this.name = name;
        this.auteurs = auteurs;
        this.editeur = editeur;
        this.publishingYear = publishingYear;
        this.numberPages = numberPages;
        this.prix = prix;
        this.coverType = coverType;
    }

    @Override
    public String toString() {
        return "Book {id=" + id +
                ", name='" + name + '\'' +
                ", auteurs=" + Arrays.toString(auteurs) +
                ", editeur=" + editeur +
                ", publishingYear=" + publishingYear +
                ", numberPages=" + numberPages +
                ", prix=" + prix +
                ", coverType=" + coverType +
                '}';
    }
}
