package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookService {

    public static Book[] filterBooksByAuthor(Auteur auteur, Book[] books) {
        Book[] booksFilter = new Book[0];
        List<Book> listBooks = new ArrayList<>(Arrays.asList(booksFilter));
/*
        for (Book book : books) {
            for (int i = 0; i < book.auteurs.length; i++) {
                if (book.auteurs[i].equals(auteur)) {
                    listBooks.add(book);
                }
            }
        }
*/

        //Méthode avec stream()
        listBooks = Arrays.stream(books)
                .filter(b -> Arrays.asList(b.auteurs)
                        .contains(auteur))
                .toList();

        booksFilter = listBooks.toArray(booksFilter);

        return booksFilter;
    }

    public static Book[] filterBooksByPublisher(Editeur editeur, Book[] books) {
        Book[] booksFilter = new Book[0];
        List<Book> listBooks = new ArrayList<>(Arrays.asList(booksFilter));
        for (Book book : books) {
            if (book.editeur.equals(editeur)) {
                listBooks.add(book);
            }
        }
        booksFilter = listBooks.toArray(booksFilter);
        return booksFilter;
    }

    public static Book[] filterBooksAfterSpecifiedYear(int yearFromInclusively, Book[] books) {
        Book[] booksFilter = new Book[0];
        List<Book> listBooks = new ArrayList<>(Arrays.asList(booksFilter));
        for (Book book : books) {
            if (book.publishingYear >= yearFromInclusively) {
                listBooks.add(book);
            }
        }
        booksFilter = listBooks.toArray(booksFilter);
        return booksFilter;
    }

}
