package org.example;

public class Editeur {

    public int id;
    private static int count = 1;
    public String publisherName;

    public Editeur(String publisherName) {
        this.id = count++;
        this.publisherName = publisherName;
    }

    @Override
    public String toString() {
        return "Editeur {id=" + id + ", publisherName='" + publisherName + '\'' + '}';
    }
}
