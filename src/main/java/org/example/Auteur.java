package org.example;

public class Auteur {

    public int id;
    private static int count = 1;
    public String firstname;
    public String lastname;

    public Auteur(String firstname, String lastname) {
        this.id = count++;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "Auteur { id=" + id + ", firstname='" + firstname + '\'' + ", lastname='" + lastname + '\'' + '}';
    }
}
