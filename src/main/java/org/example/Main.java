package org.example;

import java.math.BigDecimal;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Auteur jon = new Auteur("Jon", "Johnson");
        Auteur william = new Auteur("William", "Wilson");
        Auteur walter = new Auteur("Walter", "Peterson");
        Auteur craig = new Auteur("Craig", "Gregory");

        Editeur editeur1 = new Editeur("Publisher_1");
        Editeur editeur2 = new Editeur("Publisher_2");
        Editeur editeur3 = new Editeur("Publisher_3");

        Book[] livres = new Book[]{
                new Book("Book_1", new Auteur[]{jon}, editeur1, 1990,
                        231, BigDecimal.valueOf(24.99), CoverType.Relie),
                new Book("Book_2", new Auteur[]{jon, william}, editeur2, 2000,
                        120, BigDecimal.valueOf(14.99), CoverType.Relie),
                new Book("Book_3", new Auteur[]{walter}, editeur1, 1997,
                        350, BigDecimal.valueOf(34.99), CoverType.Broche),
                new Book("Book_4", new Auteur[]{craig}, editeur3, 1992,
                        185, BigDecimal.valueOf(19.99), CoverType.Relie)
        };

        System.out.println("Method filterBooksByAuthor : ");
        System.out.println(Arrays.toString(BookService.filterBooksByAuthor(walter, livres)));
        System.out.println("\nMethod filterBooksByPublisher : ");
        System.out.println(Arrays.toString(BookService.filterBooksByPublisher(editeur1, livres)));
        System.out.println("\nMethod filterBooksAfterSpecifiedYear : ");
        System.out.println(Arrays.toString(BookService.filterBooksAfterSpecifiedYear(1997, livres)));

    }
}